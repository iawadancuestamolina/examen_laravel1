<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Type\Integer;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        for ($i = 0; $i <= 10; $i++) {
        DB::table('productos')->insert([
            'nombre' => Str::random(10),
            'precio' => rand(1,50),
            'descripcion' => Str::random(100),
        ]);
    }
    }
}
